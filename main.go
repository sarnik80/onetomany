package main

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/lib/pq"
)

type Post struct {
	Id      int
	Content string
	Author  string

	Comments []Comment
}

type Comment struct {
	Id      int
	Content string
	Author  string
	Post    *Post
}

var (
	Db *sql.DB
)

func init() {

	var err error

	connectionSTR := "postgres://root:j06ar8KkODkxhOon0y2LEe2y@arthur.iran.liara.ir:30135/s?sslmode=disable"

	Db, err = sql.Open("postgres", connectionSTR)

	if err != nil {

		panic(err)
	}
}

func main() {

	post := Post{Content: "Hello World!", Author: "Sra"}
	post.Create()

	comm := Comment{Content: "Good Post!", Author: "joe", Post: &post}

	comm.Create()

	readPost, err := GetPost(post.Id)

	fmt.Println(err)
	fmt.Println(readPost)

	fmt.Println(readPost.Content)
	fmt.Println(readPost.Comments)

}

func (com *Comment) Create() (err error) {

	if com.Post == nil {
		err = errors.New("Post not found")

		return
	}

	query := "insert into comments (content , author , post_id)  values ($1, $2, $3) returning id;"

	err = Db.QueryRow(query, com.Content, com.Author, com.Post.Id).Scan(&com.Id)

	return

}

func GetPost(id int) (post Post, err error) {

	post = Post{}

	query := "select id , content , author  from posts where id = $1;"

	err = Db.QueryRow(query, id).Scan(&post.Id, &post.Content, &post.Author)

	query = "select id , content , author from comments where post_id = $1;"

	rows, err := Db.Query(query, post.Id)

	if err != nil {
		return
	}

	for rows.Next() {
		comment := Comment{Post: &post}

		err = rows.Scan(&comment.Id, &comment.Content, &comment.Author)

		if err != nil {
			return
		}

		post.Comments = append(post.Comments, comment)
	}

	rows.Close()
	return

}

func (post *Post) Create() (err error) {

	stmt := "insert into posts (content , author) values ($1 , $2) returning id;"
	err = Db.QueryRow(stmt, post.Content, post.Author).Scan(&post.Id)

	return

}
