# Go and SQL relationships

In this project we want to look at how we can use standard CRUD f
for two related tables . base on Go Web Programming book (Sau Sheong Chang)

## Requirements

* Postgresql 14.8
* go 1.18.1
* github.com/lib/pq v1.10.9


## Installation


```bash
go get  github.com/lib/pq
```

## Descriptions

* There are 4  ways of relating a record to other recprds 

    1. One to one (has one)

    2. One to many (has many)

    3. Many to one (belongs to)

    4. Many to many

    We'll use a one-to-many relationship : one post has many comments .

    =>  one to many  ==  inverse of many to one relationship .

* Setting Up The Database 
    
    we have setup.sql script with 2 tables .

    we use these commands on the terminal :

    ```bash
    psql -f setup.sql  -h (HostName)  -p (Port)  -U (Username) -W (Database name)
    ```
    حذف کردن جدول هایی که به هم دیگه مرتبط هستن هم که یه مقداری پیچیده تر هست با ید به این توجه کنیم 

## One-to-many relationship

* First look st the Post and Comment Struct :

```go
type Post struct {
    Id int
    Content string
    Author string
    Comments []Comment
}
```

```go
type Comment struct {

    Id int
    Content string
    Author string
    Post *Post
}
```

* Comment has a field named Post that's a pointer to a Post struct 

* The Comments field in the Post struct in a slice , which is really a pointer to an array .

* در واقع ما امدیم و ریلیشن رو با استفاده از پوینتر برقرار کردیم و یک کپی از  همون پست نمیخواهیم بلکه  میخواهیم اشاره کنیم  به همون پست  



* Create a Comment 

```go
func (comment *Comment) Create() (err error) {

    /*
    
        before you create the relationship between the comment and the post , you need to 
        make sure that the Post exists ?!
    */

 if comment.Post == nil {
 err = errors.New("Post not found")
 return
 }


 // adding post_id will create the relationship
 
 err = Db.QueryRow("insert into comments (content, author, post_id)
 ➥ values ($1, $2, $3) returning id", comment.Content, comment.Author,
 ➥ comment.Post.Id).Scan(&comment.Id)


 return
}
```

* GetPost function   


```go

/*
خوب حالا که اومدیم و ریلیشن رو برقرار کردیم بین پست و کامنت هاش 
 ما میخواهیم ک بتونیم به پست و همه ی کامنت هاش رو ریتریو کنیم  که این متد دقیقا همین کار رو میکند 

*/


func GetPost(id int) (post Post, err error) {

	post = Post{}

	query := "select id , content , author  from posts where id = $1;"

	err = Db.QueryRow(query, id).Scan(&post.Id, &post.Content, &post.Author)


    // اینحا  میایم و فقط کامنت هایی رو برمیگردانیم ک آیدی پست اونها با آیدی این پست یکی باشد 

	query = "select id , content , author from comments where post_id = $1;"

	rows, err := Db.Query(query, post.Id)

	if err != nil {
		return
	}



    // we get all the comments related to this post and iterate through it .

	for rows.Next() {

         // اینحا اون کامنت ما پوینت میکنه به اون پست

		comment := Comment{Post: &post}

		err = rows.Scan(&comment.Id, &comment.Content, &comment.Author)

		if err != nil {
			return
		}

		post.Comments = append(post.Comments, comment)
	}

	rows.Close()
	return

}

```


خوب حالا همونطور که دیدیم توی این مثال خیلی ایجاد رابطه سخت نبود ولی اگر برنامه های  وب ما بزرگ تر بشوند خوب یک مقداری ممکن هست که سخت و حوصله سرر بشود 
و خوب توی پروژه ی بعدی خواهیم دید چطوری یه سری ابزار کارمون رو راحت تر میکنن هر موقع اون رو هم خوندم همراه با ریدمی براتون میزارمش اینجا منتظر ما باشید


- [seeOther](https://gitlab.com/sarnik80/realationalmappers)

## License

[MIT](https://choosealicense.com/licenses/mit/)